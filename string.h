#pragma once


typedef char                                                string_char_t;
typedef unsigned int                                        string_id_t;
typedef void*                                               string_user_data_t;

typedef struct          string                              string_t;
typedef struct          string_range                        string_range_t;
typedef struct          string_data                         string_data_t;
typedef struct          string_data_adapter                 string_data_adapter_t;
typedef struct          string_data_visitor                 string_data_visitor_t; 

typedef struct          string_accessor_interface           string_accessor_interface;
typedef struct          string_accessor_user                string_accessor_user_t;
typedef struct          string_accessor_range               string_accessor_range_t;
typedef struct          string_accessor_data                string_accessor_data_t;
typedef struct          string_accessor_data_adapter        string_accessor_data_adapter_t;
typedef struct          string_accessor_data_visitor        string_accessor_data_visitor_t;

typedef struct          string_list                         string_list_t;
typedef struct          string_list_adapter                 string_list_adapter_t;
typedef struct          string_list_visitor                 string_list_visitor_t;

typedef struct          string_adapter_user                 string_adapter_user_t;
typedef struct          string_visitor_user                 string_visitor_user_t;



struct string
{
    const string_char_t* begin;
    const string_char_t* end;
};

struct string_range
{
    string_t* begin;
    string_t* end;
};

struct string_data
{
    string_id_t     id;
    string_range_t  range;
};

struct string_data_adapter
{
    string_t&   (*add)      (string_user_data_t&, string_data_t&,       string_id_t&);
    string_t*   (*find)     (string_user_data_t&, string_data_t&, const string_id_t&);   
    bool        (*remove)   (string_user_data_t&, string_data_t&, const string_id_t&);
};

struct string_data_visitor
{
    bool(*prepare)  (string_user_data_t&, string_data_t&, string_range_t&);
    bool(*visit)    (string_user_data_t&, string_range_t&);
};

struct string_accessor_interface
{
    bool(*set)(string_user_data_t&, string_t&);
    bool(*set)(string_user_data_t&, string_t&);
};

struct string_accessor_user
{
    string_user_data_t              instance;
    string_accessor_interface_t     interface;
};

struct string_accessor_range
{
    string_accessor_user_t* begin;
    string_accessor_user_t* end;
};

struct string_accessor_data
{
    string_id_t                      id;
    string_accessor_range_t   range;
};

struct string_accessor_data_adapter
{
    string_accessor_user_t&     (*add)      (string_user_data_t&, string_accessor_data_t&,       string_id_t&);
    string_accessor_user_t*     (*find)     (string_user_data_t&, string_accessor_data_t&, const string_id_t&);   
    bool                        (*remove)   (string_user_data_t&, string_accessor_data_t&, const string_id_t&);
};

struct string_accessor_data_visitor
{
    bool(*prepare)  (string_user_data_t&, string_accessor_data_t&, string_accessor_range_t&);
    bool(*visit)    (string_user_data_t&, string_accessor_range_t&);
};

struct string_list
{
    string_data_t             data;
    string_accessor_user_t    accessor;
};

struct string_list_adapter
{
    string_data_adapter_t             data;
    string_accessor_data_adapter_t    accessor;
};

struct string_list_visitor
{
    string_data_visitor_t             data;
    string_accessor_data_visitor_t    accessor;
};


struct string_adapter_user
{
    string_user_data_t      instance;
    string_list_adapter_t   interface;
};

struct string_visitor_user
{
    string_user_data_t      instance;
    string_list_visitor_t   interface;
};


string_t&                   string_add                (string_adapter_user_t& self, string_data_t& data,                string_id_t& id);
string_t*                   string_find               (string_adapter_user_t& self, string_data_t& data,          const string_id_t& id);   
bool                        string_remove             (string_adapter_user_t& self, string_data_t& data,          const string_id_t& id);

string_accessor_user_t&     string_accessor_add       (string_adapter_user_t& self, string_accessor_data_t& data,       string_id_t& id);
string_accessor_user_t*     string_accessor_find      (string_adapter_user_t& self, string_accessor_data_t& data, const string_id_t& id);   
bool                        string_accessor_remove    (string_adapter_user_t& self, string_accessor_data_t& data, const string_id_t& id);


bool string_visit (string_visitor_user_t& self, string_data_t           &data);
bool string_visit (string_visitor_user_t& self, string_accessor_data_t  &data);