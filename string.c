#include "string.h"


string_t& string_add (
    string_adapter_user_t   &self, 
    string_data_t           &data,
    string_id_t             &id
) {
    return self.interface.data.add(self.instance, data, id);
}
    
string_t* string_find (
    string_adapter_user_t   &self, 
    string_data_t           &data,
    const string_id_t       &id
); {
    return self.interface.data.find(self.instance, data, id);
}  
    
bool string_remove (
    string_adapter_user_t   &self, 
    string_data_t           &data,
    const string_id_t       &id
) {
    return self.interface.data.remove(self.instance, data, id);
}


string_accessor_user_t& string_accessor_add (
    string_adapter_user_t   &self, 
    string_accessor_data_t  &data,
    string_id_t             &id
) {
    return self.interface.accessor.add(self.instance, data, id);
}
    
string_accessor_user_t* string_accessor_find (
    string_adapter_user_t   &self, 
    string_accessor_data_t  &data,
    const string_id_t       &id
); {
    return self.interface.accessor.find(self.instance, data, id);
}  
    
bool string_accessor_remove (
    string_adapter_user_t   &self, 
    string_accessor_data_t  &data, 
    const string_id_t       &id
) {
    return self.interface.accessor.remove(self.instance, data, id);
}


bool string_visit (
    string_visitor_user_t   &self, 
    string_data_t           &data
) {
    bool b = true;
    
    string_range_t visit_range;
    
    while (b && self.interface.data.prepare(self.instance, data, visit_range))
    {
        b = self.interface.data.visit(self.instance, visit_range);
    }
    
    return b;
}

bool string_visit (
    string_visitor_user_t       &self,  
    string_accessor_data_t      &data
) {
    bool b = true;
    
    string_accessor_range_t   visit_range;
    
    while (b && self.interface.accessor.prepare(self.instance, data, visit_range))
    {
        b = self.interface.accessor.visit(self.instance, visit_range);
    }
    
    return b;
}